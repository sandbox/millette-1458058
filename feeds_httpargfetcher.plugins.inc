<?php

/**
 * @file
 * CTools plugins declarations.
 */

/**
 * Break out for feeds_httpargfetcher_feed_plugins().
 */

function _feeds_httpargfetcher_feeds_plugins() {
  return array('FeedsHTTPArgFetcher' => array(
    'name' => 'HTTP Arg Fetcher',
    'description' => 'Download content from a template URL.',
    'handler' => array(
      'parent' => 'FeedsHTTPFetcher', // This is the key name, not the class name.
      'class' => 'FeedsHTTPArgFetcher',
      'file' => 'FeedsHTTPArgFetcher.inc',
      'path' => drupal_get_path('module', 'feeds_httpargfetcher') . '/plugins',
    ),
  ));
}
