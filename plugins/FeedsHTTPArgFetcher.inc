<?php

/**
 * @file
 * Home of the FeedsHTTPArgFetcher class.
 */

/**
 * Fetches data via HTTP based on a template url.
 */
class FeedsHTTPArgFetcher extends FeedsHTTPFetcher {
  /**
   * Override parent::configDefaults().
   */
  public function configDefaults() {
    return array_merge(parent::configDefaults(), array(
      'template_url' => '',
    ));
  }

  /**
   * Override parent::configForm().
   */
  public function configForm(&$form_state) {
    return array_merge(parent::configForm($form_state), array('template_url' => array(
      '#type' => 'textfield',
      '#title' => t('Template URL'),
      '#description' => t('TODO--REWRITE---Enter the URL of a designated PubSubHubbub hub (e. g. superfeedr.com). If given, this hub will be used instead of the hub specified in the actual feed.'),
      '#default_value' => $this->config['template_url'],
    )));
  }

  /**
   * Expose source form.
   */
  public function sourceForm($source_config) {
    $form = array();
    $form['source'] = array(
      '#type' => empty($source_config['source']) ? 'hidden' : 'textfield',
      '#title' => t('URL'),
      '#description' => t('Enter a feed URL.'),
      '#default_value' => empty($source_config['source']) ? $this->config['template_url'] : $source_config['source'],
      '#maxlength' => NULL,
      '#required' => TRUE,
    );
    return $form;
  }
}

